#VulnBoard

VulnBoard is my project for the [Cyber Security Base](https://cybersecuritybase.github.io/) MOOC.

VulnBoard provides a simple message board system based on political opinions. Messages are only visible to users that
agree with some of the political opinions chosen by the sender of the message.

VulnBoard is licensed under the MIT license.

**Important note:** Running VulnBoard on your computer is not recommended due to intentionally insufficient security
features. While the application is not intended to cause or enable any damage, the author is not responsible for any
damage resulting from its use.

##Vulnerabilities

- [XSS](https://www.owasp.org/index.php/Top_10_2013-A3-Cross-Site_Scripting_%28XSS%29). Any inputs to the application
  are not validated and messages are output without escaping, which allows users to force any HTML, including `<script>`
  elements, to be viewed by other users' browsers.
- [Insecure Direct Object References](https://www.owasp.org/index.php/Top_10_2013-A4-Insecure_Direct_Object_References).
  Messages sent by users are only intended to be shown to a given audience, but due to missing authentication anyone can
  access any message through its URL.
- [Sensitive Data Exposure](https://www.owasp.org/index.php/Top_10_2013-A6-Sensitive_Data_Exposure). The application
  stores and handles unencrypted credentials and payment information. This information is also accessible by
  administrators (and due to another vulnerability, to anyone) via a debug page at `/debug`.
- [Missing Function Level Access Control](https://www.owasp.org/index.php/Top_10_2013-A7-Missing_Function_Level_Access_Control).
  The application's debug feature does not require authentication, and is accessible at `/debug` even without
  logging in, even though the link is only shown to users logged in as administrators.
- [CSRF](https://www.owasp.org/index.php/Top_10_2013-A8-Cross-Site_Request_Forgery_%28CSRF%29). The message deletion
  feature does not contain a CSRF check, so a malicious website can force a client to send a request to delete their
  messages.

##Testing

The project is built in IntelliJ IDEA, and thus contains the files required to edit, build and run it in IDEA.

The application is a regular Spring Boot application based on the given template. It can be downloaded and run like any
other such application.