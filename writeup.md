__Application:__ https://bitbucket.org/Pietu1998/vulnboard

Run like the exercises. Administrator credentials: username `pete`, password `qwerty`

VulnBoard provides a simple message board system based on political opinions. Messages are only visible to users that
agree with some of the political opinions chosen by the sender of the message.

##Issue: Stored XSS in usernames

###Identification

1. Enter the Sign up form from the login page.
2. Type `<script>alert("XSS");</script>` as the username and fill out the other fields.
3. Sign up.
4. Log in with the username `<script>alert("XSS");</script>` and the password you chose.
5. Proceed to the Send message form.
6. Fill out the fields.
7. Send the message.
8. Observe the JavaScript alert executing on the message list page for every user that is able to view the message.

###Impact

A user may be able to execute arbitrary code on another user's browser. Checking all political opinions in the message
makes every user see the message and thus be targeted. The 32 character limit makes this vulnerability slightly harder
to exploit in practice. Inserting HTML in a field is a very common check, but the attack requires multiple steps and
might not be automatically discovered; however, a human is likely to discover it.

###Fixing

Change the `th:utext` attributes whose values can contain the usernames to `th:text`. These include

- message.html line 4
- message.html line 9
- messages.html line 17

Replacing all unnecessary `th:utext` attributes with `th:text` is also extremely recommended.

##Issue: Stored XSS in message bodies

###Identification

1. Log in as any user.
2. Proceed to the Send message form.
3. Type `<script>alert("XSS");</script>` as the message and fill out the other fields.
4. Send the message.
5. Observe the JavaScript alert executing on the message list page for every user that is able to view the message.

###Impact

A user may be able to execute arbitrary code on another user's browser. Checking all political opinions in the message
makes every user see the message and thus be targeted. Inserting HTML in a field is a very common check, so the
vulnerability is likely to be discovered.

###Fixing

Change the `th:utext` attributes whose values can contain parts of the message body to `th:text`. These include

- message.html line 12
- messages.html line 20

Replacing all unnecessary `th:utext` attributes with `th:text` is also extremely recommended.

##Issue: Insecure Direct Object References in message viewer

###Identification

1. Log in as any user.
2. Proceed to the Send message form.
3. Check some, but not all of the political opinions, and fill out the other fields.
4. Send the message.
5. Click on the message heading to view the full message.
6. Take note of the URL.
7. Log out and proceed to the Sign up form.
8. Check some political opinions, but none of the ones picked in step 3, and fill out the other fields.
9. Sign up.
10. Log in with the credentials you chose.
11. Observe that you are not able to see the message sent earlier.
12. Enter the URL from step 6.
13. Observe that you are now able to access the message.

###Impact

A user willing to see all messages can just create an account with all political opinions checked, but this
vulnerability allows them to do it with their current account. This vulnerability is likely to be discovered, since it
can be found by simply fuzzing URLs or manual testing.

###Fixing

Add a check to MessageController.viewMessage, checking that the viewing user has one of the given political opinions.
Example code (add after MessageController.java line 81):

    if (user.getOpinions().stream().noneMatch(message.getOpinions()::contains))
        throw new AccessDeniedException();

##Issue: Sensitive Data Exposure

###Identification (method 1)

1. Log in as the administrator (`pete`/`qwerty`).
2. Enter the URL `/debug`.
3. Observe that you can view the details of all users.

###Identification (method 2)

1. Open the database of the application outside the application (e.g. through the H2 console).
2. Observe that you can view the details of all users.

###Impact

A data breach may lead to credentials, payment details and other information of users leaking. Exploiting this
vulnerability requires some kind of data breach to happen (see the next issue). Discovering the issue requires either a
data breach or inspection of the code.

###Fixing

Change the password encoder used (SecurityConfiguration.java lines 41, 44-57) to a proper hashing encoder, such as a
`BCryptPasswordEncoder`. Encrypt the payment information; a possible way might be encryption using an asymmetric
cipher's public key, so the information could only be decrypted when needed by the party possessing the private key.

##Issue: Missing Function Level Access Control in debug function

###Identification

1. Enter the URL `/debug` while not logged in or logged in as any user.
2. Observe that you can view the details of all users.

###Impact

A user may be able to view the information of other users with no authentication. Exploiting this vulnerability is
trivial. Paired with the previous issue the impact is critical. This vulnerability may be discovered via trying
different common debug URLs automatically or manually.

###Fixing

The recommended fix would be to remove the debug feature entirely. If this is not possible, authentication should be
added to the debug feature. Add the argument `@AuthenticationPrincipal Account user` to `DebugController.viewUsers`
(DebugController.java line 20) and add the following code to the start of the method:

    if (!user.isAdmin())
        throw new AccessDeniedException();

##Issue: Cross-Site Request Forgery in message deletion

###Identification

1. Log in as any user.
2. Proceed to the Send message form.
3. Fill out the fields.
4. Send the message.
5. Take note of the URL of the link in the message's heading. This URL is public and the same for all users.
6. Append `/delete` to the URL to get a URL such as `/messages/4/delete`.
7. Enter the URL.
8. Observe that the message was deleted through entering a URL

###Impact

A user may be able to force another user to delete their messages by entering a crafted link. Entering the link can be
done via a link or even an image on a malicious page, so exploiting only requires that the attacker gets the victim to
click a link or view a web page controlled by the attacker.

###Fixing

Change the deletion feature to use DELETE requests instead of GET. This can be done by either including a form with
`method="DELETE"` and a submit link/button in each post, or using a single form for the pages and updating and sending
it via JavaScript. Then, on MessageController.java line 86 change `RequestMethod.GET` to `RequestMethod.DELETE`. The
CSRF handling will be automatically performed by Spring.