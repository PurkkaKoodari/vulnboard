package net.pietu1998.cybersecuritybase.project1.validation;

import net.pietu1998.cybersecuritybase.project1.BeanUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {
    private String checkedField, comparedField, message;

    @Override
    public void initialize(FieldMatch fieldMatch) {
        checkedField = fieldMatch.checkedField();
        comparedField = fieldMatch.comparedField();
        message = fieldMatch.message();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        try {
            Object first = BeanUtil.readProperty(o, checkedField);
            Object second = BeanUtil.readProperty(o, comparedField);
            boolean valid = first == null ? second == null : first.equals(second);
            if (valid)
                return true;
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(message).addPropertyNode(checkedField).addConstraintViolation();
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
