package net.pietu1998.cybersecuritybase.project1.validation;

import net.pietu1998.cybersecuritybase.project1.BeanUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Calendar;

public class ExpirationDateValidator implements ConstraintValidator<ExpirationDate, Object> {
    private String yearField, monthField, yearMessage, monthMessage;

    @Override
    public void initialize(ExpirationDate expirationDate) {
        monthField = expirationDate.monthField();
        yearField = expirationDate.yearField();
        monthMessage = expirationDate.monthMessage();
        yearMessage = expirationDate.yearMessage();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        try {
            int month = (Integer) BeanUtil.readProperty(o, monthField);
            int year = (Integer) BeanUtil.readProperty(o, yearField);
            Calendar now = Calendar.getInstance();
            int nowYear = now.get(Calendar.YEAR);
            int nowMonth = now.get(Calendar.MONTH);
            boolean valid = true;
            if (year < nowYear) {
                constraintValidatorContext.buildConstraintViolationWithTemplate(yearMessage).addPropertyNode(yearField).addConstraintViolation();
                valid = false;
            }
            if (month < 1 || month > 12 || year == nowYear && month < nowMonth) {
                constraintValidatorContext.buildConstraintViolationWithTemplate(monthMessage).addPropertyNode(monthField).addConstraintViolation();
                valid = false;
            }
            if (!valid)
                constraintValidatorContext.disableDefaultConstraintViolation();
            return valid;
        } catch (Exception e) {
            return false;
        }
    }
}
