package net.pietu1998.cybersecuritybase.project1.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = FieldMatchValidator.class)
@Documented
public @interface FieldMatch {
    String message() default "fields must match";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default { };

    String checkedField();

    String comparedField();
}
