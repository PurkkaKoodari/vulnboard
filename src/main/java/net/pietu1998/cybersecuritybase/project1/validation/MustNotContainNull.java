package net.pietu1998.cybersecuritybase.project1.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MustNotContainNullValidator.class)
@Documented
public @interface MustNotContainNull {

    String message() default "must not be null";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default { };

}
