package net.pietu1998.cybersecuritybase.project1.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ExpirationDateValidator.class)
@Documented
public @interface ExpirationDate {
    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default { };

    String monthField();

    String yearField();

    String monthMessage();

    String yearMessage();
}
