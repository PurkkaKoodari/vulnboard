package net.pietu1998.cybersecuritybase.project1.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

public class MustNotContainNullValidator implements ConstraintValidator<MustNotContainNull, Collection<?>> {
    @Override
    public void initialize(MustNotContainNull mustNotContainNull) {
    }

    @Override
    public boolean isValid(Collection<?> collection, ConstraintValidatorContext constraintValidatorContext) {
        return collection != null && !collection.contains(null);
    }
}
