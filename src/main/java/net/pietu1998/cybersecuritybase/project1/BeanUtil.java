package net.pietu1998.cybersecuritybase.project1;

import org.springframework.beans.BeanUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class BeanUtil {
    private BeanUtil() {
    }

    public static Object readProperty(Object o, String field) throws IllegalAccessException, InvocationTargetException {
        PropertyDescriptor descriptor = BeanUtils.getPropertyDescriptor(o.getClass(), field);
        Method method = descriptor.getReadMethod();
        if (method == null)
            throw new RuntimeException("no read method");
        return method.invoke(o);
    }
}
