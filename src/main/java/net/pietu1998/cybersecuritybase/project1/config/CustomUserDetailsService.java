package net.pietu1998.cybersecuritybase.project1.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;

import net.pietu1998.cybersecuritybase.project1.domain.Account;
import net.pietu1998.cybersecuritybase.project1.domain.Message;
import net.pietu1998.cybersecuritybase.project1.domain.Opinion;
import net.pietu1998.cybersecuritybase.project1.repository.AccountRepository;
import net.pietu1998.cybersecuritybase.project1.repository.MessageRepository;
import net.pietu1998.cybersecuritybase.project1.repository.OpinionRepository;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private OpinionRepository opinionRepository;

    @Autowired
    private MessageRepository messageRepository;

    @PostConstruct
    public void init() {
        opinionRepository.save(Arrays.stream(new String[] {
                "I support Trump",
                "I support Hillary",
                "I support Putin",
                "Pi is equal to 3",
                "Pi is equal to 4",
                "I identify as an attack helicopter",
                "I am too scared to express any opinion"
        }).map(text -> {
            Opinion opinion = new Opinion();
            opinion.setContent(text);
            return opinion;
        }).collect(Collectors.toList()));

        Account account = new Account();
        account.setUsername("pete");
        account.setPassword("qwerty");
        account.setEmail("pete@maansiirtofirma.fi");
        account.setCreditCardNo("4206913376668008");
        account.setCreditCardExpMonth(10);
        account.setCreditCardExpYear(1998);
        account.setCreditCardCsc("437");
        account.setOpinions(new HashSet<>(opinionRepository.findAll()));
        account.setAdmin(true);
        accountRepository.save(account);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findByUsername(username);
        if (account == null) {
            throw new UsernameNotFoundException("No such account: " + username);
        }
        return account;
    }
}
