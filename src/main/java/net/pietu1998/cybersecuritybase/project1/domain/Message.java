package net.pietu1998.cybersecuritybase.project1.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Message extends AbstractPersistable<Long> {

    @OneToOne
    private Account poster;

    @Column(length = 10000)
    private String content;

    @ManyToMany(targetEntity = Opinion.class, fetch = FetchType.EAGER)
    private Set<Opinion> opinions;

    public Message() {
    }

    public Message(Account poster, PostMessage message) {
        this.poster = poster;
        this.content = message.getContent();
        this.opinions = message.getOpinions();
    }

    public String summary() {
        String text = this.content;
        String[] sentences = text.split("[!?.] ");
        if (sentences.length > 1)
            text = text.substring(0, sentences[0].length() + 1) + "\u00a0";
        else
            text = text.trim();
        if (text.length() > 100)
            return text.substring(0, 100).trim() + "\u2026";
        else
            return text;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Account getPoster() {
        return poster;
    }

    public void setPoster(Account poster) {
        this.poster = poster;
    }

    public Set<Opinion> getOpinions() {
        return opinions;
    }

    public void setOpinions(Set<Opinion> opinions) {
        this.opinions = opinions;
    }
}