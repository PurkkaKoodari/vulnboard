package net.pietu1998.cybersecuritybase.project1.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
public class Account extends AbstractPersistable<Long> implements UserDetails {

    @Column(unique = true, length = 32)
    private String username;
    @Column(length = 256)
    private String email;
    @Column(length = 8)
    private String password;

    @Column(length = 16)
    private String creditCardNo;
    private int creditCardExpMonth, creditCardExpYear;
    @Column(length = 3)
    private String creditCardCsc;

    @ManyToMany(targetEntity = Opinion.class, fetch = FetchType.EAGER)
    private Set<Opinion> opinions;

    private boolean admin;

    @OneToMany(mappedBy = "poster")
    private List<Message> messages;

    public Account() {
    }

    public Account(Signup signup) {
        this.username = signup.getUsername();
        this.email = signup.getEmail();
        this.password = signup.getPassword();
        this.creditCardNo = signup.getCardNo();
        this.creditCardExpMonth = signup.getExpMonth();
        this.creditCardExpYear = signup.getExpYear();
        this.creditCardCsc = signup.getCsc();
        this.opinions = signup.getOpinions();
    }

    public String quote() {
        Random random = new Random();
        Opinion[] list = opinions.toArray(new Opinion[opinions.size()]);
        return list[random.nextInt(list.length)].getContent();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (isAdmin())
            return Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"), new SimpleGrantedAuthority("ROLE_ADMIN"));
        else
            return Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreditCardNo() {
        return creditCardNo;
    }

    public void setCreditCardNo(String creditCardNo) {
        this.creditCardNo = creditCardNo;
    }

    public int getCreditCardExpMonth() {
        return creditCardExpMonth;
    }

    public void setCreditCardExpMonth(int creditCardExpMonth) {
        this.creditCardExpMonth = creditCardExpMonth;
    }

    public int getCreditCardExpYear() {
        return creditCardExpYear;
    }

    public void setCreditCardExpYear(int creditCardExpYear) {
        this.creditCardExpYear = creditCardExpYear;
    }

    public String getCreditCardCsc() {
        return creditCardCsc;
    }

    public void setCreditCardCsc(String creditCardCsc) {
        this.creditCardCsc = creditCardCsc;
    }

    public Set<Opinion> getOpinions() {
        return opinions;
    }

    public void setOpinions(Set<Opinion> opinions) {
        this.opinions = opinions;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isAdmin() {
        return admin;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
