package net.pietu1998.cybersecuritybase.project1.domain;

import net.pietu1998.cybersecuritybase.project1.validation.ExpirationDate;
import net.pietu1998.cybersecuritybase.project1.validation.FieldMatch;
import net.pietu1998.cybersecuritybase.project1.validation.MustNotContainNull;
import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.*;
import java.util.Set;

@FieldMatch(comparedField = "password", checkedField = "confirmPassword", message = "passwords must match")
@ExpirationDate(monthField = "expMonth", yearField = "expYear", monthMessage = "please enter a valid expiration month",
        yearMessage = "please enter a valid expiration year")
public class Signup {
    @NotNull
    @Size.List({
            @Size(min = 1, message = "please enter a username"),
            @Size(max = 32, message = "maximum 32 characters"),
    })
    private String username;

    @NotNull
    @Size.List({
            @Size(min = 1, message = "please enter an email"),
            @Size(max = 256, message = "maximum 256 characters"),
    })
    @Email
    private String email;

    @NotNull
    @Size(max = 8, message = "maximum 8 letters")
    @Pattern(regexp = "^[a-zA-Z]{0,8}$", message = "only letters allowed")
    private String password;

    @NotNull
    private String confirmPassword;

    @NotNull
    @CreditCardNumber
    private String cardNo;

    @NotNull
    private int expMonth;

    @NotNull
    private int expYear;

    @NotNull
    @Pattern(regexp = "^\\d{3}$", message = "please enter a 3 digit CSC")
    private String csc;

    @NotNull
    @Size(min = 1, message = "please check at least one")
    @MustNotContainNull(message = "invalid opinion selected")
    private Set<Opinion> opinions;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    public String getCsc() {
        return csc;
    }

    public void setCsc(String csc) {
        this.csc = csc;
    }

    public Set<Opinion> getOpinions() {
        return opinions;
    }

    public void setOpinions(Set<Opinion> opinions) {
        this.opinions = opinions;
    }
}
