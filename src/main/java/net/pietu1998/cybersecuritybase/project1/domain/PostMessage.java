package net.pietu1998.cybersecuritybase.project1.domain;

import net.pietu1998.cybersecuritybase.project1.validation.MustNotContainNull;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

public class PostMessage {

    @NotNull
    @Size(min = 1, max = 10000, message = "please enter 1 to 10000 characters")
    private String content;

    @NotNull
    @Size(min = 1, message = "please check at least one")
    @MustNotContainNull(message = "invalid opinion selected")
    private Set<Opinion> opinions;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Set<Opinion> getOpinions() {
        return opinions;
    }

    public void setOpinions(Set<Opinion> opinions) {
        this.opinions = opinions;
    }
}
