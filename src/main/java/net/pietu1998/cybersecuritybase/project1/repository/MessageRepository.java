package net.pietu1998.cybersecuritybase.project1.repository;

import net.pietu1998.cybersecuritybase.project1.domain.Message;
import net.pietu1998.cybersecuritybase.project1.domain.Opinion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findDistinctMessagesByOpinionsInOrderByIdDesc(Set<Opinion> opinion);
}