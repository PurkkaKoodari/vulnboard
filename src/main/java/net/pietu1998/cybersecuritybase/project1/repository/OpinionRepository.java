package net.pietu1998.cybersecuritybase.project1.repository;

import net.pietu1998.cybersecuritybase.project1.domain.Opinion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OpinionRepository extends JpaRepository<Opinion, Long> {
    Opinion findByContent(String content);
}
