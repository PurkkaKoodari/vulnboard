package net.pietu1998.cybersecuritybase.project1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import net.pietu1998.cybersecuritybase.project1.domain.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Account findByUsername(String username);

}
