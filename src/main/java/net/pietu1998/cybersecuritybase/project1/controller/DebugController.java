package net.pietu1998.cybersecuritybase.project1.controller;

import net.pietu1998.cybersecuritybase.project1.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DebugController {

    @Autowired
    private AccountRepository accountRepository;

    @RequestMapping(value = "/debug", method = RequestMethod.GET)
    public String viewUsers(Model model) {
        model.addAttribute("users", accountRepository.findAll());
        return "debug";
    }

}
