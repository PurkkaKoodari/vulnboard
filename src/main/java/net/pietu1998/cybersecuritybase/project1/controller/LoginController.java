package net.pietu1998.cybersecuritybase.project1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginForm(HttpServletRequest request, Model model) {
        model.addAttribute("error", request.getParameter("error"));
        model.addAttribute("logout", request.getParameter("logout"));
        model.addAttribute("signup", request.getParameter("signup"));
        return "login";
    }

}
