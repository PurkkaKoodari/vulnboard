package net.pietu1998.cybersecuritybase.project1.controller;

import net.pietu1998.cybersecuritybase.project1.repository.AccountRepository;
import net.pietu1998.cybersecuritybase.project1.repository.OpinionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import net.pietu1998.cybersecuritybase.project1.domain.Account;
import net.pietu1998.cybersecuritybase.project1.domain.Signup;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class SignupController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private OpinionRepository opinionRepository;

    private void populateFields(Model model) {
        int nowYear = Calendar.getInstance().get(Calendar.YEAR);
        List<Integer> years = IntStream.rangeClosed(nowYear, nowYear + 10).mapToObj(i -> i).collect(Collectors.toList());
        model.addAttribute("years", years);
        model.addAttribute("opinionValues", opinionRepository.findAll());
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String loadForm(Model model) {
        populateFields(model);
        model.addAttribute("signup", new Signup());
        return "signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String submitForm(@Valid Signup signup, BindingResult result, Model model) {
        if (result.hasErrors()) {
            populateFields(model);
            return "signup";
        }
        accountRepository.save(new Account(signup));
        return "redirect:/login?signup";
    }

}
