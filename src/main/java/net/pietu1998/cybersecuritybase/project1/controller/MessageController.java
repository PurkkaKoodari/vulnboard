package net.pietu1998.cybersecuritybase.project1.controller;

import net.pietu1998.cybersecuritybase.project1.AccessDeniedException;
import net.pietu1998.cybersecuritybase.project1.ItemNotFoundException;
import net.pietu1998.cybersecuritybase.project1.domain.Account;
import net.pietu1998.cybersecuritybase.project1.domain.Message;
import net.pietu1998.cybersecuritybase.project1.domain.Opinion;
import net.pietu1998.cybersecuritybase.project1.domain.PostMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import net.pietu1998.cybersecuritybase.project1.repository.MessageRepository;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Controller
public class MessageController {

    @Autowired
    private MessageRepository messageRepository;

    @RequestMapping("/")
    public String defaultMapping() {
        return "redirect:/messages";
    }

    private void populateFields(Model model, Account user) {
        List<Opinion> opinions = new ArrayList<>(user.getOpinions());
        opinions.sort(Comparator.comparingLong(AbstractPersistable::getId));
        model.addAttribute("userOpinions", opinions);
    }

    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    public String viewMessages(Model model, HttpServletRequest request, @AuthenticationPrincipal Account user) {
        model.addAttribute("admin", request.isUserInRole("ADMIN"));
        List<Message> messages = messageRepository.findDistinctMessagesByOpinionsInOrderByIdDesc(user.getOpinions());
        model.addAttribute("messages", messages);
        model.addAttribute("user", user);
        return "messages";
    }

    @RequestMapping(value = "/messages/new", method = RequestMethod.GET)
    public String messageForm(Model model, @AuthenticationPrincipal Account user) {
        populateFields(model, user);
        PostMessage message = new PostMessage();
        message.setOpinions(user.getOpinions());
        model.addAttribute("postMessage", message);
        return "newmessage";
    }

    @RequestMapping(value = "/messages/new", method = RequestMethod.POST)
    public String postMessage(@Valid PostMessage message, BindingResult result, Model model, @AuthenticationPrincipal Account user) {
        if (message.getOpinions().stream().anyMatch(opinion -> !user.getOpinions().contains(opinion))) {
            result.addError(new FieldError("signup", "opinions", "invalid opinion selected"));
        }
        if (result.hasErrors()) {
            populateFields(model, user);
            return "newmessage";
        }
        messageRepository.save(new Message(user, message));
        return "redirect:/messages";
    }

    @RequestMapping(value = "/messages/{id}", method = RequestMethod.GET)
    public String viewMessage(@PathVariable("id") long id, Model model, @AuthenticationPrincipal Account user) {
        Message message = messageRepository.findOne(id);
        if (message == null)
            throw new ItemNotFoundException();
        model.addAttribute("message", message);
        return "message";
    }

    @RequestMapping(value = "/messages/{id}/delete", method = RequestMethod.GET)
    public String deleteMessage(@PathVariable("id") long id, @AuthenticationPrincipal Account user) {
        Message message = messageRepository.findOne(id);
        if (message == null)
            throw new ItemNotFoundException();
        if (!message.getPoster().equals(user))
            throw new AccessDeniedException();
        messageRepository.delete(message);
        return "redirect:/messages";
    }

}